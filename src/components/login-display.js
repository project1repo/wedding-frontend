


export default function LoginDisplay(){
    if(localStorage.getItem('fname')){
        return(<h4 style={{textAlign:"right"}}>Logged In As: {localStorage.getItem('fname')} {localStorage.getItem('lname')}</h4>)
    }else{
        return(<h4 style={{textAlign:"right"}}>Not Logged In</h4>)
    }
}