

export default function MessagesTable(props){



    const messages = props.messages;

    return(<table className='table'>
        <thead><tr><th>Sender</th><th>Recipient</th><th>Note</th></tr></thead>
        <tbody>
            {messages.map(m => <tr key={m.key}>
                <td>{m.sender}</td>
                <td>{m.recipient}</td>
                <td>{m.note}</td>
            </tr>)}
        </tbody>
    </table>)
}