


export default function ExpenseTable(props){

    const expenses = props.expenses;

    return(<table className='table'>
        <thead><tr><th>ID</th><th>Reason</th><th>Amount</th><th>Wedding ID</th></tr></thead>
        <tbody>
            {expenses.map(e => <tr key={e.eId}>
                <td>{e.eId}</td>
                <td>{e.reason}</td>
                <td>{e.amount}</td>
                <td>{e.weddingId}</td>
            </tr>)}
        </tbody>
    </table>)
}