import axios from "axios";
import { SyntheticEvent, useState } from "react";
import {useRef} from "react";
import NavBar from "./navbar";


export default function LoginPage(){


    const emailInput = useRef(null);
    const passInput = useRef(null);

    async function Login(event){
        const email = emailInput.current.value;
        const password = passInput.current.value;
        try{
            const response = await axios.get(`https://tactile-carver-322819.uc.r.appspot.com/users/${email}/verify`);
            const newRequest = {"email":email, "password":password}
            const response2 = await axios.patch('https://tactile-carver-322819.uc.r.appspot.com/users/login', newRequest);
            localStorage.setItem("fname", response2.data.fname)
            localStorage.setItem("lname", response2.data.lname)
            localStorage.setItem("email", email)
            alert("logged in as " + response2.data.fname + " " + response2.data.lname);
        }catch{
            alert("Invalid username or password")
        }
    }

    return(<div style={{textAlign:"center"}}>
        <h1>Log in</h1>
        <div>
        <input placeholder="Email" ref={emailInput}></input>
        <input placeholder="Password" ref={passInput} type="password"></input>
        </div>
        <button onClick={Login}>Login</button>
    </div>)
}