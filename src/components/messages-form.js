import axios from "axios";
import { SyntheticEvent, useState } from "react";
import {useRef} from "react";
import NavBar from './navbar'
import MessagesTable from "./messages-table";


export default function MessagesForm(){

    const senderInput = useRef(null);
    const recInput = useRef(null);

    const newSender = useRef(null);
    const newRec = useRef(null);
    const newNote = useRef(null);

    const [messages,setMessages] = useState([]);
    const [message,setMessage] = useState();


    async function getMessages(event){

        const sender = senderInput.current.value;
        const recipient = recInput.current.value;
        try{
        
            if(sender && recipient){
                await axios.get(`https://tactile-carver-322819.uc.r.appspot.com/users/${sender}/verify`);
                await axios.get(`https://tactile-carver-322819.uc.r.appspot.com/users/${recipient}/verify`);
                const response = await axios.get(`https://message-backend-dot-tactile-carver-322819.uc.r.appspot.com/messages?sender=${sender}&recipient=${recipient}`);
                setMessages(response.data);
            }else if(sender){
                await axios.get(`https://tactile-carver-322819.uc.r.appspot.com/users/${sender}/verify`);
                const response = await axios.get(`https://message-backend-dot-tactile-carver-322819.uc.r.appspot.com/messages?sender=${sender}`)
                setMessages(response.data);
            }else if(recipient){
                await axios.get(`https://tactile-carver-322819.uc.r.appspot.com/users/${recipient}/verify`);
                const response = await axios.get(`https://message-backend-dot-tactile-carver-322819.uc.r.appspot.com/messages?recipient=${recipient}`)
                setMessages(response.data);
            }else{
                const response = await axios.get('https://message-backend-dot-tactile-carver-322819.uc.r.appspot.com/messages');
                setMessages(response.data);
            }
        }catch{
            alert("Invalid Input");
        }
    }

    async function postMessage(event){
        const sender = newSender.current.value;
        const recipient = newRec.current.value;
        const note = newNote.current.value;
        var testDate = 0;
        testDate = +new Date;
        alert(testDate)
        if((sender && recipient && note) && (sender.includes("@") && recipient.includes("@"))){
            try{
                const newMessage = {note:note, sender:sender, recipient:recipient}
                await axios.post('https://message-backend-dot-tactile-carver-322819.uc.r.appspot.com/messages', newMessage)
            }catch{
                alert("Invalid Input")
            }
        }else{
            alert("Please provide a valid input for each field")
        }
    }



    return(<div style={{textAlign: "center"}}>

        <h1>Messages</h1>
        <div>
            <input placeholder="sender" ref={senderInput}></input>
            <input placeholder="recipient" ref={recInput}></input>
        </div>
        <button onClick={getMessages}>View Messages</button>
        <MessagesTable messages={messages}></MessagesTable>
        <div>
            <input placeholder="sender" ref={newSender}></input>
            <input placeholder="recipient" ref={newRec}></input>
            <div>
                <input placeholder="message" ref={newNote} style={{width:346}}></input>
            </div>
            <button onClick={postMessage}>Send Message</button>
        </div>
        
    </div>)

}