import axios from "axios";
import { SyntheticEvent, useState } from "react";
import {useRef} from "react";
import ExpenseTable from "./expense-table";


export default function ExpenseForm(){

    const [expenses,setExpenses] = useState([]);
    const [file,setFile] = useState();
    const deleteExpIn = useRef(null);

    const reasonInput = useRef(null);
    const amountInput = useRef(null);
    const wIdInput = useRef(null);
    const eIdInput = useRef(null);



    async function getExpenses(event){
        const response = await axios.get('http://35.227.125.183:3000/expenses');
        setExpenses(response.data);
    }

    

    async function postExpense(event){
        const reason = reasonInput.current.value;
        const amount = amountInput.current.value;
        const wId = wIdInput.current.value;
        const newExpense = {eId:0, reason:reason, amount:amount, weddingId:wId}
        const response = await axios.post('http://35.227.125.183:3000/expenses', newExpense);
        alert(file)
        if(file){
            alert("flag2")
            var fullPath = (document.getElementById('fileUpload')).value
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            var fileExt = filename.split('.').pop();
                filename = filename.split('.').slice(0, -1).join('.')
            }
            const fileSize = (document.getElementById('fileUpload')).files[0];
            if(fileSize.size > 10485760){
                alert("Uploaded file must be less than 10 mb!")
                return;
            }
            const fileContent = await toBase64(file);
            const newFile = {name:filename, extension:fileExt, content:fileContent}
            const response2 = await axios.post('https://us-east1-tactile-carver-322819.cloudfunctions.net/file-upload', newFile)
            alert(response2);
        }
    }
    

    async function putExpense(event){
        const reason = reasonInput.current.value;
        const amount = amountInput.current.value;
        const wId = wIdInput.current.value;
        const eId = eIdInput.current.value;
        const newExpense = {eId:0, reason:reason, amount:amount, weddingId:wId}
        const response = await axios.put(`http://35.227.125.183:3000/expenses/${eId}`, newExpense);
    }


    async function deleteExpense(event){
        const expID = deleteExpIn.current.value;
        const response = await axios.delete(`http://35.227.125.183:3000/expenses/${expID}`);
    }


    function onFileChange(file){
        alert("flag onFileChange")
        setFile(file);
        alert("flag onFileChange2")
    }


    function toBase64(file){
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
              let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
              if ((encoded.length % 4) > 0) {
                encoded += '='.repeat(4 - (encoded.length % 4));
              }
              resolve(encoded);
            };
            reader.onerror = error => reject(error);
          });
      }






    return(<div style={{textAlign: "center"}}>
        <h1>Expense Planner</h1>
        <button onClick={getExpenses}>Show Expenses</button>
        <ExpenseTable expenses={expenses}></ExpenseTable>
        <h2>Create a New Expense</h2>
        <input placeholder="Reason" ref={reasonInput}></input>
        <input placeholder="Amount" ref={amountInput}></input>
        <input placeholder="Wedding ID" ref={wIdInput}></input>
        
        <button onClick={postExpense}>Create Expense</button>
        <div>
        <input type="file" accept="image/png, image/jpg" id="fileUpload"  onChange={(e) => onFileChange(e.target.files[0])}></input>
        </div>
        <div>
            <h3>Replace an existing expense</h3>
            <input placeholder="ID" ref={eIdInput}></input>
            <button onClick={putExpense}>Replace</button>
        </div>
        <h2>Delete an Expense</h2>
        <input placeholder="Expense ID" ref={deleteExpIn}></input>
        <button onClick={deleteExpense}>Delete Expense</button>
        

    </div>)
}