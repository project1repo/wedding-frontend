import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import LoginDisplay from "./login-display";

export default function NavBar(){
    return(<Navbar bg="dark" variant="dark">
        <Container className='table'>
                <Nav style={{textAlign:'center'}}>
                    
                    <Nav.Link href="Login" style={{padding:"50px"}}>Login Page</Nav.Link>
                    <Nav.Link href="Wedding" style={{padding:"50px"}}>Wedding Page</Nav.Link>
                    <Nav.Link href="Expense" style={{padding:"50px"}}>Expense page</Nav.Link>
                    <Nav.Link href="Messages" style={{padding:"50px"}}>Messages Page</Nav.Link>
                    <LoginDisplay></LoginDisplay>
                </Nav>
        </Container>
    </Navbar>)
}