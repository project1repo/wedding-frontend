import React from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './components/login-page';
import WeddingForm from './components/wedding-form'
import MessagesForm from './components/messages-form';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import NavBar from './components/navbar'
import ExpenseForm from './components/expense-form';
import './index.css'

ReactDOM.render(
  <React.StrictMode>
      <NavBar></NavBar>
      <Router>
      
      <Route path="/Login">
        <LoginPage></LoginPage>
      </Route>
      
      <Route path="/Wedding">
        <WeddingForm></WeddingForm>
      </Route>

      <Route path="/Expense">
        <ExpenseForm></ExpenseForm>
      </Route>

      <Route path="/Messages">
        <MessagesForm></MessagesForm>
      </Route>

      

    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

